<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/test',function(){
    return "tes masuk";
});

//post
Route::get('posts','PostController@index');
Route::post('posts','PostController@store');
Route::put('/posts/{post_id}','PostController@update');
Route::delete('/posts/{post_id}','PostController@destroy');
Route::get('/posts/{post_id}','PostController@show');

//role
Route::get('roles','RolesController@index');
Route::post('roles','RolesController@store');
Route::put('/roles/{role_id}','RolesController@update');
Route::delete('/roles/{role_id}','RolesController@destroy');
Route::get('/roles/{role_id}','RolesController@show');

//comments
Route::get('comments','CommentsController@index');
Route::post('comments','CommentsController@store');
Route::put('/comments/{comments_id}','CommentsController@update');
Route::delete('/comments/{comments_id}','CommentsController@destroy');
Route::get('/comments/{comments_id}','CommentsController@show');
