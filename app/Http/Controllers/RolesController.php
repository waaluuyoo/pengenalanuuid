<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Roles::latest()->get();

        return response()->json([
            'success'=>true,
            'message'=>'data daftar roles berhasil di tampilkan',
            'data'=>$roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Roles::create([
            'name'     => $request->name
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Roles Created',
                'data'    => $roles  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Roles Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $roles = Roles::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $roles 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $roles = Roles::findOrfail($id);

        if($roles) {

            //update post
            $roles->update([
                'name'     => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $roles  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $roles = Roles::findOrfail($id);

         if($roles) {
 
             //delete post
             $roles->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Post Deleted',
             ], 200);
 
         }
 
         //data post not found
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);
    }
}
